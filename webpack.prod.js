const { merge } = require('webpack-merge');
const { DefinePlugin } = require('webpack');
const common = require('./webpack.common');

module.exports = (env) => merge(common, {
  mode: 'production',
  plugins: [
    new DefinePlugin({
      BACKEND_URL: JSON.stringify(env.APP_HOST),
      BACKEND_PORT: JSON.stringify(env.APP_PORT),
      REDUX_DEV_TOOLS: JSON.stringify('false'),
    }),
  ],
});
