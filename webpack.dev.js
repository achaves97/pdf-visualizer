const { merge } = require('webpack-merge');
const { DefinePlugin } = require('webpack');
const common = require('./webpack.common');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval-source-map',
  plugins: [
    new DefinePlugin({
      BACKEND_URL: JSON.stringify('http://192.168.0.6'),
      BACKEND_PORT: JSON.stringify(':3000'),
      REDUX_DEV_TOOLS: JSON.stringify('false'),
    }),
  ],
  devServer: {
	  historyApiFallback: true,
  },
});
