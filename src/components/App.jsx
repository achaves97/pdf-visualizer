import React from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import MainEditor from './container/MainEditor';

const brandTheme = createMuiTheme({
  palette: {
    primary: {
      main: '#0099CC',
    },
    secondary: {
      main: '#CCFFCC',
    },
  },
});

function App() {
  return (
    <MuiThemeProvider theme={brandTheme}>
      <CssBaseline />
      <MainEditor />
    </MuiThemeProvider>
  );
}

export default App;
