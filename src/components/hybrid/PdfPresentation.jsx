import React, { useRef, useEffect, useState } from 'react';
import clsx from 'clsx';
import {
  Button, IconButton, Tooltip, CircularProgress,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';

const useStyles = makeStyles((theme) => ({
  defaultHideTool: {
    backgroundColor: theme.palette.primary.main,
  },
  selectedHideTool: {
    backgroundColor: theme.palette.secondary.main,
  },
  defaultButtonIcon: {
    color: 'white',
  },
  button: {
    margin: theme.spacing(1),
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
}));

function PdfPresentation({ zoomOut, zoomIn, processingPDf }) {
  const classes = useStyles();

  const pdfContainerRef = useRef(null);
  const [anonymizing, setAnonymizing] = useState(false);

  /* useEffect(() => {
    if (anonymizing) {
      if (pdfContainerRef.current) {
        pdfContainerRef.current.className = 'anonymization-enabled';
      }
    } else if (pdfContainerRef.current) {
      pdfContainerRef.current.className = '';
    }
  }, [anonymizing]); */

  function handleAnonimyzation() {
    setAnonymizing(!anonymizing);
  }

  return (
    <div>
      <div id="pdf-toolbar" className={classes.flexRow}>
        <div>
          <Tooltip title="Procesar archivo con OCR">
            <Button size="small" className={classes.button} variant="contained" color="primary">
              OCR
            </Button>
          </Tooltip>
          <Tooltip title="Anonimizar">
            <IconButton
              id="anonymize"
              size="small"
	      an="false"
            >
              <VisibilityOffIcon className={classes.defaultButtonIcon} />
            </IconButton>
          </Tooltip>
          <input id="anonymization-input" className="hidden" readOnly value={false} />
        </div>
        <div>
          <Tooltip title="Alejar">
            <IconButton
              id="zoom-out"
              size="small"
              className={clsx(classes.defaultHideTool, classes.button)}
              // onClick={zoomOut}
            >
              <ZoomOutIcon className={classes.defaultButtonIcon} />
            </IconButton>
          </Tooltip>
          <Tooltip title="Acercar">
            <IconButton
              id="zoom-in"
              size="small"
              className={clsx(classes.defaultHideTool, classes.button)}
              // onClick={zoomIn}
            >
              <ZoomInIcon className={classes.defaultButtonIcon} />
            </IconButton>
          </Tooltip>
        </div>
      </div>
      <div
        className={clsx(anonymizing ? 'anonymization-enabled' : '')}
        ref={pdfContainerRef}
        id="pdf-container"
      />
    </div>
  );
}

export default PdfPresentation;
