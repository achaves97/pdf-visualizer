import React, { useState, useEffect, useCallback } from 'react';
import update from 'immutability-helper';
import MainEditorPresentational from '../presentational/MainEditor';
import {
  getDocTypes, saveDoc, getDocument, anonymize,
} from '../../services/editor';
import {
  loadPdf, pdfCleanup, rectCleanup, rectDraw,
} from '../../controllers/pdfController';
import InfoDialog from '../presentational/InfoDialog';

const BASE64_MARKER = ';base64,';

function convertDataURIToBinary(b64) {
  const bin = atob(b64);
  return bin;
}

function MainEditorContainer() {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [dialogText, setDialogText] = useState('');
  const [fileSaved, setFileSaved] = useState(false);
  const [loadedFile, setLoadedFile] = useState(false);
  const [fileArrayValue, setFileArrayValue] = useState(undefined);
  const [zoom, setZoom] = useState(1.5);
  const [loadedDocTypes, setLoadedDocTypes] = useState(false);
  const [docTypes, setDocTypes] = useState([]);
  const [selectedDocType, setSelectedDocType] = useState('');
  const [demoFieldsValue, setDemoFieldsValue] = useState([]);
  const [anonymizationAreas, setAnonymizationAreas] = useState([]);
  const [processingPDf, setProcessingPdf] = useState(true);

  useEffect(() => {
    const docData = getDocument();
    docData.then((r) => {
      console.log(r);
      const binaryData = convertDataURIToBinary(r.pdf_original);
      setFileArrayValue(binaryData);
      setLoadedFile(true);
      loadPdf(binaryData, zoom, anonymizationAreas, pushNewArea, setProcessingPdf);
    });
    const types = getDocTypes();
    types.then((r) => {
      setDocTypes(r);
      setLoadedDocTypes(true);
    });
  }, []);

  useEffect(() => {
    const docTypeFields = docTypes.find((value) => value.name === selectedDocType);
    if (docTypeFields !== undefined) {
      console.log('Not undefined ', docTypeFields);
      const docTypeFieldsWithValue = docTypeFields.data.map((v) => ({ ...v, value: '' }));
      console.log('Setting doc type array ', docTypeFieldsWithValue);
      setDemoFieldsValue(docTypeFieldsWithValue);
    }
  }, [selectedDocType]);

  useEffect(() => {
    /* if (fileArrayValue !== undefined) {
      pdfCleanup();
      console.log('Previous call scale ', zoom);
      loadPdf(fileArrayValue, zoom, anonymizationAreas, pushNewArea, setProcessingPdf);
    } */
    const newAnonymizationAreas = anonymizationAreas.map((area) => ({
      ...area,
      x: area.x + area.x * 0.25,
      width: area.width + area.width * 0.25,
      y: area.y + area.y * 0.25,
      height: area.height + area.height * 0.25,
    }));
    setAnonymizationAreas(newAnonymizationAreas);
  }, [zoom]);

  const pushNewArea = (newArea) => {
    console.log('Old anonymizationAreas ', anonymizationAreas);
    console.log('New anonymizationAreas', [...anonymizationAreas, newArea]);
    setAnonymizationAreas([...anonymizationAreas, newArea]);
  };

  useEffect(() => {
    console.log('Anonymization areas have changed');
    console.log(anonymizationAreas);
    if (anonymizationAreas.length > 0) {
      // pdfCleanup();
      // loadPdf(fileArrayValue, zoom, anonymizationAreas, pushNewArea, setProcessingPdf);
      rectCleanup();
      rectDraw(anonymizationAreas, pushNewArea);
    }
  }, [anonymizationAreas]);

  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      const reader = new FileReader();

      reader.onabort = () => console.log('file reading was aborted');
      reader.onerror = () => console.log('file reading has failed');
      reader.onload = () => {
        // Do whatever you want with the file contents
        // const binaryStr = reader.result;
        const typedarray = new Uint8Array(reader.result);
        setLoadedFile(true);
        loadPdf(typedarray, zoom, anonymizationAreas, pushNewArea, setProcessingPdf);
        setFileArrayValue(typedarray);
      };
      reader.readAsArrayBuffer(file);
    });
  }, []);

  const zoomOut = () => {
    setZoom(zoom - 0.25);
  };

  const zoomIn = () => {
    setZoom(zoom + 0.25);
  };

  const createDemoFieldsValueHandler = (index) => (e) => {
    const { value } = e.target;
    const updatedObj = update(demoFieldsValue, { [index]: { value: { $set: value } } });
    setDemoFieldsValue(updatedObj);
  };

  const createSetDemoFieldFromSelection = (index) => () => {
    const selection = document.getSelection();
    if (selection !== null && selection !== undefined) {
      const updatedObj = update(demoFieldsValue, {
        [index]: {
          value: { $set: selection.toString() },
        },
      });
      setDemoFieldsValue(updatedObj);
    }
  };

  const handleSubmit = async () => {
    const fCanvasesNodes = document.querySelectorAll('canvas[id^="f-canvas-"]');
    console.log(fCanvasesNodes);
    for (let i = 0; i < fCanvasesNodes.length; i++) {
      const fCanvas = fCanvasesNodes[i].fabric;
      console.log(fCanvas);

      fCanvas.getObjects().map((o) => {
        console.log('Fabric object');
        console.log(o);
      });
    }
    // const fCanvases = fCanvasesNodes.map(node => (node.fabric))
    // console.log(fCanvases)
    /* setFileSaved(false);
    setDialogText('Guardando');
    setDialogOpen(true);
    /*
    const docTypeFields = docTypes.find((value) => (value.name === selectedDocType));
    const keys = Object.keys(demoFieldsValue);
    const data = keys.map((key) => ({
      name: docTypeFields[key],
      value: demoFieldsValue[key],
    })); */
    /* console.log(demoFieldsValue);
    const normalizedAnonymization = anonymizationAreas.map((area) => {
      const {
        x, y, height, width, viewport, page,
      } = area;
      return {
        x: x / viewport.width,
        y: y / viewport.height,
        width: width / viewport.width,
        height: height / viewport.height,
        page,
      };
    });
    console.log('Normalized', normalizedAnonymization);
    const anonyRes = await anonymize({
      pdfOriginal: btoa(fileArrayValue),
      anonymizationAreas: normalizedAnonymization,
    });
    console.log('Anonymizarion service ', anonyRes);
    const res = await saveDoc({
      demoFieldsValue,
      asignado: 11223344,
      doc_procesado: anonyRes.pdfProcesado,
    });
    setDialogText(res.message);
    setFileSaved(true); */
  };

  return (
    <>
      <MainEditorPresentational
        loadedFile={loadedFile}
        loadedDocTypes={loadedDocTypes}
        docTypes={docTypes}
        selectedDocType={selectedDocType}
        setSelectedDocType={setSelectedDocType}
        demoFieldsValue={demoFieldsValue}
        createDemoFieldsValueHandler={createDemoFieldsValueHandler}
        createSetDemoFieldFromSelection={createSetDemoFieldFromSelection}
        onDrop={onDrop}
        zoomOut={zoomOut}
        zoomIn={zoomIn}
        handleSubmit={handleSubmit}
        processingPDf={processingPDf}
      />
      <InfoDialog
        open={dialogOpen}
        onClose={() => {
          if (fileSaved) {
            setDialogOpen(false);
          }
        }}
        dialogText={dialogText}
        fileSaved={fileSaved}
      />
    </>
  );
}

export default MainEditorContainer;
