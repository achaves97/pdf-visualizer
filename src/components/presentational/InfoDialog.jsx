import React from 'react';
import { Dialog, Typography, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

const useStyles = makeStyles((theme) => ({
  dialog: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
}));

function InfoDialog({
  open, onClose, dialogText, fileSaved,
}) {
  const classes = useStyles();
  return (
    <Dialog open={open} onClose={onClose}>
      <div className={classes.dialog}>
        <Typography align="center" variant="h5" color="primary">
          {dialogText}
        </Typography>
        {fileSaved ? (
          <CheckCircleIcon color="primary" />
        ) : (
          <CircularProgress />
        )}
      </div>
    </Dialog>
  );
}

export default InfoDialog;
