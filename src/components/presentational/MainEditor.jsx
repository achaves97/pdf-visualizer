import React from 'react';
import {
  Typography,
  Grid,
  TextField,
  IconButton,
  Tooltip,
  Select,
  MenuItem,
  Divider,
  CircularProgress,
  Button,
} from '@material-ui/core';
import clsx from 'clsx';
import { useDropzone } from 'react-dropzone';
import { makeStyles } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';
import SplitPane from 'react-split-pane';
import Pane from 'react-split-pane/lib/Pane';
import PdfPresentation from '../hybrid/PdfPresentation';

const useStyles = makeStyles((theme) => ({
  mainFlexColumn: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'stretch',
    alignItems: 'stretch',
  },
  rightColumn: {
    height: '95vh',
  },
  centeredFlexColumn: {
    display: 'flex',
    flexDirection:
	  'column',
    justifyContent: 'center',
  },
  rowFlex: {
    display: 'flex',
    flexDirection: 'row',
  },
  templateInput: {
    marginTop: theme.spacing(1),
    flex: 1,
  },
  title: {
    height: '5vh',
  },
  fileSelector: {
    height: '92vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderStyle: 'dotted',
    margin: theme.spacing(1),
    // backgroundColor: theme.palette.secondary.main,
  },
  docSelector: {
    marginRight: theme.spacing(1),
  },
  divider: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  textCenterAligned: {
    textAlign: 'center',
  },
  saveButton: {
    margin: theme.spacing(1),
  },
  formDiv: {
    overflowY: 'auto',
  },
  secondaryPane: {
    padding: theme.spacing(1),
    overflowX: 'auto',
  },
}));

function MainEditorPresentational({
  loadedFile,
  fieldValue,
  loadedDocTypes,
  docTypes,
  setFieldValue,
  setFieldFromSelection,
  selectedDocType,
  setSelectedDocType,
  demoFieldsValue,
  createDemoFieldsValueHandler,
  createSetDemoFieldFromSelection,
  onDrop,
  zoomOut,
  zoomIn,
  handleSubmit,
  processingPDf,
}) {
  const classes = useStyles();

  return (
    <SplitPane split="vertical">
      <Pane minSize="20%" initialSize="75%" maxSize="95%">
        <PdfPresentation processingPDf={processingPDf} zoomOut={zoomOut} zoomIn={zoomIn} />
        {/* loadedFile ? (
        ) : (
          <CircularProgress />
        ) */}
      </Pane>
      <Pane className={classes.secondaryPane}>
        <Typography variant="h6" color="primary">
          Seleccionar tipo documental
        </Typography>
        {loadedDocTypes ? (
          <Select
            className={classes.docSelector}
            variant="outlined"
            value={selectedDocType}
            onChange={(e) => {
              setSelectedDocType(e.target.value);
            }}
          >
            {docTypes.map((docType) => (
              <MenuItem key={docType.name} value={docType.name}>
                {docType.name}
              </MenuItem>
            ))}
          </Select>
        ) : (
          <div className={classes.textCenterAligned}>
            <CircularProgress />
          </div>
        )}
        <Divider className={classes.divider} />
        <Typography variant="h6" color="primary">
          Formulario
        </Typography>
        {demoFieldsValue.length === 0 ? (
          <Typography variant="body1" color="primary">
            Seleccione un tipo documental
          </Typography>
        ) : (
          <>
            <div className={classes.formDiv}>
              {demoFieldsValue.map((dataField, index) => (
                <div className={classes.rowFlex}>
                  <TextField
                    required
                    className={classes.templateInput}
                    label={dataField.name}
                    variant="outlined"
                    fullWidth
                    multiline
                    value={dataField.value}
                    onChange={createDemoFieldsValueHandler(index)}
                  />
                  <Tooltip title="Asociar selección">
                    <IconButton onClick={createSetDemoFieldFromSelection(index)}>
                      <SaveIcon color="primary" className={classes.secondaryIcon} />
                    </IconButton>
                  </Tooltip>
                </div>
              ))}
            </div>
            <div className={classes.textCenterAligned}>
              <Button
                className={classes.saveButton}
                variant="contained"
                color="primary"
                onClick={handleSubmit}
              >
                GUARDAR
              </Button>
            </div>
          </>
        )}
      </Pane>
    </SplitPane>
  );
}

export default MainEditorPresentational;
