import * as pdfjsLib from 'pdfjs-dist';
import * as pdfjsViewer from 'pdfjs-dist/web/pdf_viewer';
import 'pdfjs-dist/web/pdf_viewer.css';

pdfjsLib.GlobalWorkerOptions.workerSrc = '../../node_modules/pdfjs-dist/build/pdf.worker.js';

// const url = 'http://localhost:3000/5786709.pdf';

const RESIZE_DELTA = 0.1;

let zoom = 0;
let pdfDoc = null;
const fabricInstances = [];

let visualizerElements = {};
function initializeVisualizer() {
  visualizerElements = {
    pdfContainer: document.getElementById('pdf-container'),
    zoomInButton: document.getElementById('zoom-in'),
    zoomOutButton: document.getElementById('zoom-out'),
    anonymizeButton: document.getElementById('anonymize'),
    anonymizeInput: document.getElementById('anonymization-input'),
  };
  if (visualizerElements.zoomInButton) {
    visualizerElements.zoomInButton.onclick = zoomIn;
  }
  if (visualizerElements.zoomOutButton) {
    visualizerElements.zoomOutButton.onclick = zoomOut;
  }
  if (visualizerElements.anonymizeButton) {
    visualizerElements.anonymizeButton.onclick = toogleAnonymize;
  }
}

function toogleAnonymize() {
  const anonymizing = visualizerElements.anonymizeInput.value === 'true';
  console.log(`Changing anonymizing to ${!anonymizing}`);
  visualizerElements.anonymizeButton.setAttribute('an', !anonymizing);
  visualizerElements.anonymizeInput.value = !anonymizing;
}

initializeVisualizer();

window.onload = initializeVisualizer;

async function renderDoc() {
  const renderPromises = [];
  console.log('Starts rerender');
  for (let i = 1; i <= pdfDoc.numPages; i++) {
    renderPromises.push(renderPage(i));
  }
  await Promise.all(renderPromises);
  console.log('Finished rerender');
}

function resizeFabric(delta) {
  console.log('On fabric resize ', delta);
  fabricInstances.forEach((fabricInstance) => {
    console.log('Fabric resizeRatio ', fabricInstance.widthRatio);
    fabricInstance.getObjects().map((o) => {
      console.log('Fabric object');
      console.log(o);
      const originalTop = o.top;
      const originalLeft = o.left;
      const newTop = originalTop * fabricInstance.heigthRatio;
      const newLeft = originalLeft * fabricInstance.widthRatio;
      o.set({
        top: newTop,
        left: newLeft,
        scaleX: o.scaleX + delta,
        scaleY: o.scaleY + delta,
      });
    });
    fabricInstance.renderAll();
  });
}

async function zoomIn() {
  const delta = RESIZE_DELTA;
  zoom += delta;
  await renderDoc();
  resizeFabric(delta);
}

async function zoomOut() {
  const delta = -RESIZE_DELTA;
  zoom += delta;
  await renderDoc();
  resizeFabric(delta);
}

function initializeFabricCanvas(pageIndex) {
  console.log(`Initilizing fabric on ${pageIndex}`);
  const canvas = new fabric.Canvas(`f-canvas-${pageIndex}`);
  const canvasNode = document.getElementById(`f-canvas-${pageIndex}`);
  canvasNode.fabric = canvas;
  canvas.on('mouse:down', (e) => {
    mousedown(e);
  });
  canvas.on('mouse:move', (e) => {
    mousemove(e);
  });
  canvas.on('mouse:up', (e) => {
    mouseup(e);
  });

  let started = false;
  let x = 0;
  let y = 0;

  /* Mousedown */
  function mousedown(e) {
    console.log('Start rect');
    if (visualizerElements.anonymizeInput.value !== 'true') {
      console.log('Not validating');
      return false;
    }
    const mouse = canvas.getPointer();
    started = true;
    x = mouse.x;
    y = mouse.y;

    const square = new fabric.Rect({
      width: 0,
      height: 0,
      left: x,
      top: y,
      fill: '#000',
      selectable: true,
    });

    canvas.add(square);
    canvas.renderAll();
    canvas.setActiveObject(square);
  }

  /* Mousemove */
  function mousemove(e) {
    if (visualizerElements.anonymizeInput.value !== 'true') {
      console.log('Not validating');
      return false;
    }
    if (!started) {
      return false;
    }

    const mouse = canvas.getPointer();

    const w = Math.abs(mouse.x - x);
    const h = Math.abs(mouse.y - y);

    if (!w || !h) {
      return false;
    }

    const square = canvas.getActiveObject();
    square.set('width', w).set('height', h);
    canvas.renderAll();
  }

  /* Mouseup */
  function mouseup(e) {
    if (visualizerElements.anonymizeInput.value !== 'true') {
      console.log('Not validating');
      return false;
    }
    console.log('On mouse up');
    if (started) {
      started = false;
      visualizerElements.anonymizeInput.value = false;
      visualizerElements.anonymizeButton.setAttribute('an', false);
    }

    const square = canvas.getActiveObject();
    canvas.discardActiveObject();
    // canvas.add(square);
    canvas.renderAll();
  }

  fabricInstances.push(canvas);
}

async function renderPage(pageIndex) {
  console.log(`Rendering on scale ${zoom} withh index ${pageIndex}`);
  const canvas = document.getElementById(`canvas-${pageIndex}`);
  const fCanvas = document.getElementById(`f-canvas-${pageIndex}`);
  const page = await pdfDoc.getPage(pageIndex);
  console.log(page);
  if (zoom === 0) {
    const viewportOne = page.getViewport({ scale: 1 });
    console.log('Vieport 1 ', viewportOne);

    const containerWidth = visualizerElements.pdfContainer.clientWidth;
    zoom = containerWidth / viewportOne.width;
    console.log('Initial zoom ', zoom);
  }
  const viewport = page.getViewport({ scale: zoom.toFixed(3) });
  console.log(viewport);
  const ctx = canvas.getContext('2d', { alpha: false });
  canvas.height = viewport.height;
  canvas.width = viewport.width;
  fCanvas.height = viewport.height;
  fCanvas.width = viewport.width;
  console.log('Setting f dimensions canvas to ', viewport);

  // Si existen al menos paneIndex elementos en el arreglo
  console.log('Before fabric validation ', fabricInstances);
  if (fabricInstances.length < pageIndex) {
    if (fabricInstances[pageIndex - 1] === undefined) {
      initializeFabricCanvas(pageIndex);
    }
  } else if (fabricInstances[pageIndex - 1]) {
    console.log('Resizing fabric canvas');
    const fabricI = fabricInstances[pageIndex - 1];
    console.log('Old fabric width ', fabricI.width);
    console.log('New fabric width ', viewport.width);
    fabricI.set({
      widthRatio: viewport.width / fabricI.width,
      heigthRatio: viewport.height / fabricI.height,
    });
    console.log('Zoom ratio ', fabricI.widthRatio);
    fabricI.setWidth(viewport.width);
    fabricI.setHeight(viewport.height);
    fabricI.renderAll();
  }

  const renderContext = {
    canvasContext: ctx,
    viewport,
  };

  page.render(renderContext);
  const textContent = await page.getTextContent();
  const textLayerDiv = document.getElementById(`pdf-text-${pageIndex}`);

  const textLayer = new pdfjsViewer.TextLayerBuilder({
    textLayerDiv,
    pageIndex,
    viewport,
    eventBus,
  });

  // console.log('Text layer ', textLayer);
  textLayer.setTextContent(textContent);

  textLayer.render();
  console.log('Page finished rendering');
}

const eventBus = new pdfjsViewer.EventBus();
async function loadPdf(typedarray) {
  const loadingTask = pdfjsLib.getDocument({ data: typedarray });
  pdfDoc = await loadingTask.promise;
  const renderPromises = [];
  for (let i = 1; i <= pdfDoc.numPages; i++) {
    // viewport = page.getViewport({ scale });

    const pageContainer = document.createElement('div');
    pageContainer.setAttribute('id', `page-${i}`);
    pageContainer.setAttribute('style', 'position: relative; margin: 0px');

    const pdfContainer = document.getElementById('pdf-container');
    pdfContainer.append(pageContainer);

    const fabricCanvasContainer = document.createElement('div');
    fabricCanvasContainer.setAttribute('id', `fabric-container-${i}`);
    pageContainer.appendChild(fabricCanvasContainer);

    const fabricCanvas = document.createElement('canvas');
    fabricCanvas.setAttribute('id', `f-canvas-${i}`);
    fabricCanvasContainer.appendChild(fabricCanvas);

    const canvas = document.createElement('canvas');
    canvas.setAttribute('id', `canvas-${i}`);

    pageContainer.appendChild(canvas);
    const textLayerDiv = document.createElement('div');
    textLayerDiv.setAttribute('class', 'textLayer');
    textLayerDiv.setAttribute('id', `pdf-text-${i}`);

    pageContainer.appendChild(textLayerDiv);

    renderPromises.push(renderPage(i));
    // renderPage(i);
  }

  await Promise.all(renderPromises);
}

export { loadPdf };
