import axios from 'axios';
import React from 'react';
import ReactDom from 'react-dom';
import AppContainer from './components/App';
import './styles/App.css';

ReactDom.render(
  <AppContainer />,
  document.getElementById('root'),
);
