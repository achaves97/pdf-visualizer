import axios from 'axios';

const URL = 'http://190.93.151.151:3008';
const ANONYMIZE_URL = 'http://190.93.151.151:3010';

async function getDocTypes() {
  let docTypes = [];
  try {
    const serviceRes = await axios.get(`${URL}/api/tipodocq`);
    docTypes = serviceRes.data.Tipos;
  } catch (er) {
    console.log(er);
  }
  return docTypes;
}

async function saveDoc(data) {
  let saveResult = {};
  try {
    const serviceRes = await axios.post(`${URL}/api/pdfs/asignado/pdf`, data);
    saveResult = serviceRes.data;
  } catch (er) {
    console.log(er);
  }
  return saveResult;
}

async function getDocument(data) {
  let saveResult = {};
  try {
    const serviceRes = await axios.post(`${URL}/api/pdfs/asignado`, {
      asignado: 11223344,
      token: 12345,
    });
    saveResult = serviceRes.data;
  } catch (er) {
    console.log(er);
  }
  return saveResult;
}

async function anonymize(data) {
  let saveResult = {};
  try {
    const serviceRes = await axios.post(`${ANONYMIZE_URL}/anonymize`, {
      ...data,
      asignado: 44112233,
      token: 12345,
    });
    saveResult = serviceRes.data;
  } catch (er) {
    console.log(er);
  }
  return saveResult;
}

export {
  getDocTypes, saveDoc, getDocument, anonymize,
};
